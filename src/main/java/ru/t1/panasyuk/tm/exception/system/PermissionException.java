package ru.t1.panasyuk.tm.exception.system;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}